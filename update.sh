#!/bin/bash
ARCHS=('noarch' 'x86' 'x86_64')
GPG_KEY="0x09EE228B"

rm -f packages.gpg
gpg --output packages.gpg --export "$GPG_KEY"
for ARCH in "${ARCHS[@]}"; do
	mkdir -p "$ARCH"/release
	mksetupini --arch "$ARCH" --inifile="$ARCH"/setup.ini --disable-check=missing-required-package,missing-depended-package --releasearea . || continue
	bzip2 <"$ARCH"/setup.ini >"$ARCH"/setup.bz2 &
	xz -6e <${ARCH}/setup.ini >${ARCH}/setup.xz &
	wait
	for f in "setup.ini" "setup.bz2" "setup.xz"; do
		rm -f "$ARCH"/"$f".sig
		gpg -u "$GPG_KEY" --output "$ARCH"/"$f".sig --sign "$ARCH"/"$f" &
	done
	wait
done

python3 -m http.server 16792

